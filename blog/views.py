from django.shortcuts import render
from .models import Post

posts=[
    {
        'author':'Rully Feb',
        'title':'Blog Post 1',
        'content':'First post content',
        'date_posted':'20 Oktober 2020'
    },
    {
        'author':'Dicky Ang',
        'title':'Blog Post 2',
        'content':'First post content',
        'date_posted':'21 Oktober 2020',
    },
]
# Create your views here.
def home(request):
    context = {
        'posts': posts,
    }
    return render(request,'blog/home.html',context)

def about(request):
    context = {
        'title': 'About'
    }
    return render(request,'blog/about.html',context)
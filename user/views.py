from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import CustomUserCreationForm
from django.contrib import auth
# Create your views here.
def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Account created successfully.. Call Admin for Login')
        return redirect('register')
    else:
        form = CustomUserCreationForm()
    return render(request, 'users/register.html', {'form': form})
"""
def login(request):
    if request.user.is_authenticated():
        return redirect('admin_page')

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)

        if user is not None:
            # correct username and password login the user
            auth.login(request, user)
            return redirect('admin_page')

        else:
            messages.error(request, 'Error wrong username/password')

    return render(request, 'users/login.html')


def logout(request):
    auth.logout(request)
    return render(request,'users/logout.html')


def admin_page(request):
    if not request.user.is_authenticated():
        return redirect('login')

    return render(request, 'users/admin_page.html')

"""
from django.shortcuts import render
from .models import Cat_Even, Evens
# Create your views here.

def evens(request):
    cat_evens = Cat_Even.objects.all()
    evenss = Evens.objects.all()
    context = {
        'cat_evens':cat_evens,
        'evenss':evenss
    }
    return render(request, 'even.html',context)
from django.db import models
from django.utils import timezone
# Create your models here.
class Cat_Even(models.Model):
    nama_cat = models.CharField(max_length=100)
    even_posted =  models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.nama_cat

class Evens(models.Model):
    name = models.CharField(max_length=100)
    nama_cat = models.ForeignKey(Cat_Even, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
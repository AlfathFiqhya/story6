from django.apps import AppConfig


class EvensConfig(AppConfig):
    name = 'evens'
